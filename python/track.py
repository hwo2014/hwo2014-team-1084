import math

BEND = 0
REGULAR = 1

class Track(object):
    def __init__(self, data):
        self.lanes = {}
        if 'lanes' in data:
            for laneData in data['lanes']:
                self.addLane(laneData)
        
        self.pieces = {}
        self.latestPiece = False #a temporary variable for connecting the track pieces
        if 'pieces' in data:
            for pieceData in data['pieces']:
                self.addPiece(pieceData)
                
        #complete the track by connecting the last piece to the first one
        self.lastPiece().connect(self.firstPiece())
                
    def addLane(self, laneData):
        if 'id' in laneData and 'distanceFromCenter' in laneData:
            self.lanesData[lane['id']] = laneData['distanceFromCenter']
    
    def addPiece(self, pieceData):
        if 'length' in pieceData or ('radius' in pieceData and 'angle' in pieceData):
            #store the new piece in the latestPiece
            self.latestPiece = Piece(pieceData, self, self.latestPiece)
            #add the new piece as the last index
            self.pieces[len(self.pieces)] = self.latestPiece
    
    def getPiece(self, i):
        if i >= 0 and i < len(self.pieces):
            return self.pieces[i]
    
    def firstPiece(self):
        return self.getPiece(0)
    
    def lastPiece(self):
        return self.getPiece(len(self.pieces))
        
class Piece(object):
    def __init__(self, data, track, prev = False):
        self.prev = False
        self.next = False
        self.switch = False
        self.track = track
        
        #connect the previous piece to this new piece
        prev.connect(self)
        
        if 'switch' in data and data['switch'] == True:
            self.switch = True
        if 'radius' in data and 'angle' in data:
            self.type = BEND
            self.radius = data['radius']
            self.angle = data['angle']
        elif ('length' in data):
            self.type = REGULAR
            self.length = data[length]
    
    def connect(self, nextPiece):
        if isinstance(prev, Piece):
            self.next = nextPiece
            nextPiece.prev = self
    
    def length(self, startLane, endLane = False):
        if (endLane == False):
            endLane = startLane
        #no calculations for the regular pieces without switching the lane (which is possible only on switch pieces)
        if self.type == REGULAR:
            if startLane == endLane:
                return self.length
            #calculate the distance between the lanes
            laneDistance = abs(self.track.lanes[startLane] - self.track.lanes[endLane])
            return math.sqrt(math.pow(laneDistance, 2) + math.pow(self.length, 2))
        
        if self.type == BEND:
            if startLane == endLane:
                #calculate the radius taking into account the lane distance from the center of the track
                r = self.radius + self.track.lanes[startLane]
                circumference = 2 * math.pi * r
                return circumference * (self.angle/360)
            
    def getNext(self):
        return self.next
    
    def getPrev(self):
        return self.prev
    
    def isSwitch(self):
        return self.switch
    
    def isBend(self):
        return self.type == BEND
    
    def isRegular(self):
        return self.type == REGULAR