import json
import socket
import sys
from random import randint
import pprint

class NoobBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.track = False;
        self.currentPosition = False;
        self.color = False;
        self.tick = False;

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_your_car(self, data):
        self.color = data['color'];
        print("Color: "+data['color']+" selected")
        
    def on_game_init(self, data):
        print("Game init")
        self.track = data['race']['track']
        
    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_car_positions(self, data):
        #get the current position
        for position in data:
            if (position['id']['color'] == self.color):
                self.currentPosition = position
        
        angle = self.currentPosition['angle']
        #detect drift
        if (self.tick % 20 == 1):
            print(self.nextBendDistance());
        if (abs(angle) > 3):
            #self.throttle(0.6* (1.1-(min(45, abs(angle))/45)) ) #drift compensation test
            self.throttle(0.6)
        elif (self.nextBendDistance() < 40):
            self.throttle(0.1)
        else:
            #full throttle if the next bend is not near
            self.throttle(1)

    def on_crash(self, data):
        print("Someone crashed")
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def nextBendDistance(self):
        pieceIndex = self.currentPosition['piecePosition']['pieceIndex']
        inPieceDistance = self.currentPosition['piecePosition']['inPieceDistance']
        pieceLeft = self.pieceLength(pieceIndex) - inPieceDistance;
        #how much of this piece is left to travel
        
        if (self.isBend(self.nextPiece(pieceIndex))):
            return pieceLeft
        #at least this distance is left to the next bend. Might be much more! (at this point it doesn't matter)
        return pieceLeft + self.track['pieces'][self.nextPiece(pieceIndex)]['length']
    
    def nextPiece(self, index):
        return (index+1) % len(self.track['pieces'])
    
    def isBend(self, index):
        return 'angle' in self.track['pieces'][index] and self.track['pieces'][index]['angle'] != 0
    
    def pieceLength(self, index):
        if ('length' in self.track['pieces'][index]):
            return self.track['pieces'][index]['length']
        if ('radius' in self.track['pieces'][index] and 'angle' in self.track['pieces'][index]):
            return self.track['pieces'][index]['radius'] * (self.track['pieces'][index]['angle']/360)
    
    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'yourCar':  self.on_your_car,
            'gameInit': self.on_game_init,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            
            #if (randint(0,100) < 1):
            #    pprint.pprint(self.gameTick)
            #    pprint.pprint(msg)
            
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                if 'gameTick' in msg:
                    self.gameTick = msg['gameTick']
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
