class Car(object):
    def __init__(self):
    	self.lastInPiecePosition = 0
        self.inPiecePosition = 0
        self.lastTick = 0
        self.tick = 0
        self.lastPiece = False
        self.piece = False
    	
    def updatePosition(self, data):
    	if 'piecePosition' in data:
    		if 'inPieceDistance' in data['piecePosition']:
                #remember the last inPiecePosition
                self.lastInPiecePosition = self.inPiecePosition
                self.inPiecePosition = data['piecePosition']['inPieceDistance']
    
    def velocity(self):
        return